/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "event.h"

#include <core/signal.h>

namespace ergo {
namespace vox {

    class Client
    {
    public:
        Client() = default;
        virtual ~Client() = default;

        // Signal for an event occurrance
        virtual core::Signal<const Event&>& event() = 0;

        // Methods for managing the client
        virtual bool register_event(const std::string& type) = 0;

    private:
        // disable copying 
        Client(const Client&) = delete; 
        Client& operator=(const Client&) = delete; 
    };

} // vox
} // ergo
