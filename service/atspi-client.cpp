/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atspi-client.h"

#include <atspi/atspi.h>

namespace ergo {
namespace vox {

class Atspi::Impl {
public:
    Impl()
    {
        auto registered = atspi_init();
        if (registered != 0 && registered != 1) {
            // LCOV_EXCL_START
            g_warning("atspi_init() returned code: %d", registered);
            // LCOV_EXCL_STOP
        }
    }

    ~Impl()
    {
        if (m_listener != nullptr) {
            g_clear_object(&m_listener);
        }

        auto leaked = atspi_exit();
        if (leaked != 0) {
            // LCOV_EXCL_START
            g_critical("atspi_exit() reported %d leaks", leaked);
            // LCOV_EXCL_STOP
        }
    }

    // Signals to propagate
    core::Signal<const Event&>& event()
    {
        return m_event;
    }

    bool register_event(const std::string& type)
    {
        if (m_listener == nullptr) {
            m_listener = atspi_event_listener_new(on_atspi_event, this,
                                                  nullptr);
        }

        GError* error = nullptr;
        auto result = atspi_event_listener_register(m_listener,
                                                    type.c_str(),
                                                    &error);
        if (error != nullptr) {
            // LCOV_EXCL_START
            g_error("Failed to registered listener for '%s': %s",
                    type.c_str(), error->message);
            g_clear_error(&error);
            // LCOV_EXCL_STOP
        }
        return result;
    }

private:
    static void on_atspi_event(AtspiEvent *atspi_event, gpointer gthis)
    {
        auto self = static_cast<Impl*>(gthis);

        Event event;
        event.type = atspi_event->type;
        event.role = static_cast<Event::Role>(atspi_accessible_get_role(atspi_event->source, nullptr));
        auto title = atspi_accessible_get_name(atspi_event->source, nullptr);
        event.title = title;

        self->m_event(event);

        g_free(title);
        g_free(atspi_event);
    }

    AtspiEventListener* m_listener = nullptr;

    core::Signal<const Event&> m_event;
}; // class Impl

Atspi::Atspi() :
    p(new Impl())
{
}

Atspi::~Atspi()
{
}

core::Signal<const Event&>& Atspi::event()
{
    return p->event();
}

bool Atspi::register_event(const std::string& type)
{
    return p->register_event(type);
}

} // vox
} // ergo
