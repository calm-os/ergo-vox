/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "engine.h"

#include <glib.h>

namespace ergo {
namespace vox {

class Engine::Impl
{
public:
    Impl(const std::shared_ptr<Client>& client,
         const std::shared_ptr<Synthesizer>& synth) :
        m_client(client),
        m_synth(synth)
    {
        m_client->event().connect([this](const Event& event) {
                if (event.title.size() == 0) {
                    return;
                }
                g_debug("Received '%s' event: %s",
                        event.type.c_str(),
                        event.title.c_str());
                if (event.type == "window:activate") {
                    m_synth->speak_string(event.title);
                }
            });
    }

    ~Impl()
    {
    }

private:
    std::shared_ptr<Client> m_client;
    std::shared_ptr<Synthesizer> m_synth;
}; // class Impl


Engine::Engine(const std::shared_ptr<Client>& client,
               const std::shared_ptr<Synthesizer>& synth) :
    p(new Impl(client, synth))
{
    client->register_event("window:activate");
}

Engine::~Engine()
{
}

} // vox
} //ergo
