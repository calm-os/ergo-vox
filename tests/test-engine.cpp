/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "engine.h"

#include "mock-client.h"
#include "mock-synth.h"

#include <gmock/gmock.h>

using namespace ergo::vox;
using namespace ::testing;

class EngineFixture : public ::testing::Test
{
};

TEST_F(EngineFixture, Engine)
{
    auto client = std::make_shared<MockClient>();
    auto synth = std::make_shared<MockSynth>();

    auto engine = std::make_shared<Engine>(client, synth);
    ASSERT_FALSE(nullptr == engine);

    EXPECT_CALL(*client, register_event(_))
        .WillOnce(Invoke([&client](const std::string&){
                    Event fake_event;
                    fake_event.type = "window:activate";
                    fake_event.title = "Testing";
                    client->m_event(fake_event);
                    return true;
                }));
    EXPECT_CALL(*synth, speak_string("Testing")).Times(1);
    client->register_event("window:activate");
}
