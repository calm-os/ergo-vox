/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "atspi-client.h"
#include "dbus-fixture.h"

#include <string>
#include <vector>

#include <gmock/gmock.h>

#include <glib.h>

using namespace ergo::vox;
using namespace ::testing;

class AtspiFixture : public DBusFixture
{
protected:
    void wait_for_registry_service()
    {
        auto on_name_appeared = [](GDBusConnection*, const char*,
                                   const char*, gpointer gloop) {
            g_main_loop_quit(static_cast<GMainLoop*>(gloop));
        };
        auto watch_name_tag = g_bus_watch_name(G_BUS_TYPE_SESSION,
                                               "org.a11y.atspi.Registry",
                                               G_BUS_NAME_WATCHER_FLAGS_NONE,
                                               on_name_appeared,
                                               nullptr,
                                               m_main_loop,
                                               nullptr);
        g_main_loop_run(m_main_loop);
        g_bus_unwatch_name(watch_name_tag);
    }

    static void spawn_bus_template(const std::string& bus_template)
    {
        static const gchar* child_argv[] {
            "python3",
            "-m", "dbusmock",
            "-t", bus_template.c_str(),
            nullptr
        };
        GError* error = nullptr;
        g_spawn_async(nullptr, (gchar**)child_argv, nullptr,
                      G_SPAWN_SEARCH_PATH, nullptr, nullptr, nullptr,
                      &error);
        g_assert_no_error(error);
        g_clear_error(&error);
    }

    void BeforeBusSetUp() override
    {
        // use a fake bus
        g_test_dbus_unset();
        m_test_bus = g_test_dbus_new(G_TEST_DBUS_NONE);
        g_test_dbus_add_service_dir(m_test_bus, DBUS_TEST_DIR);
        g_test_dbus_up(m_test_bus);

        g_debug("Bus Address: %s", g_test_dbus_get_bus_address(m_test_bus));
        // start the mock bus interfaces
        static std::vector<std::string> mock_templates {
            "org_a11y.py",
            "org_a11y_atspi_registry.py"
        };
        for (const auto &iface: mock_templates) {
            std::string mock_iface{DBUS_TEST_DIR};
            mock_iface += iface;
            spawn_bus_template(mock_iface);
        }
    }

    void SetUp() override
    {
        DBusFixture::SetUp();

        m_main_loop = g_main_loop_new(nullptr, false);

        wait_for_registry_service();
    }

    void TearDown() override
    {
        g_main_loop_unref(m_main_loop);

        DBusFixture::TearDown();

        g_clear_object(&m_test_bus);
    }

    GMainLoop* m_main_loop = nullptr;
    GTestDBus* m_test_bus = nullptr;
};

TEST_F(AtspiFixture, AtspiClient)
{
    // Test initialization
    auto client = std::make_shared<Atspi>();
    ASSERT_FALSE(nullptr == client);

    auto gmain = g_main_loop_new(nullptr, false);
    client->event().connect([&gmain](const Event& event) {
            g_warning("Event '%s' for '%s'", event.type.c_str(), event.title.c_str());
            g_main_loop_quit(static_cast<GMainLoop*>(gmain));
        });
    g_timeout_add_seconds(10, [](gpointer loop) -> gboolean {
            g_main_loop_quit(static_cast<GMainLoop*>(loop));
            return false;
        }, gmain);
    g_idle_add([](gpointer gself) -> gboolean {
            auto self = static_cast<Atspi*>(gself);
            g_debug("Registering event.");
            self->register_event("window:activate");
            return false;
        }, client.get());
    g_main_loop_run(gmain);
}
